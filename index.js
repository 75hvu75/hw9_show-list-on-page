'use strict';

const myArr = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];

const myArr2 = ['1', '2', '3', 'sea', 'user', 23];

function myList(arr, parent = document.body) {
    let container = document.createElement('ul');

    document.body.append(container);

    container.insertAdjacentHTML('beforeend', `${arr.map(i => `<li>${i}</li>`).join('')}`);
}

myList(myArr);

myList(myArr2);
